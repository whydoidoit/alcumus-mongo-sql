
var helpers = require('../../lib/query-helpers');
var queryBuilder = require('../../lib/query-builder');
var utils = require('../../lib/utils');

helpers.register('database', function(database, values, query){

  if (typeof database != 'string') throw new Error('Invalid database type: ' + typeof database);

  return utils.quoteObject(database)
});
